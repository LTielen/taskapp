<%@ page import="com.realdolmen.tasks.domain.Task" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: LTLBU70
  Date: 18/10/2019
  Time: 8:57
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8"/>
    <title>Tasks</title>
    <script type="text/javascript" src="resources/jquery-2.2.1/jquery.js"></script>
    <script type="text/javascript" src="resources/bootstrap-3.3.6/js/bootstrap.js"></script>
    <link type="text/css" rel="stylesheet" href="resources/bootstrap-3.3.6/css/bootstrap-jsf.css"/>
</head>
<body>
<div class="container">

    <h1>Task Manager</h1>
    <%
        List<Task> tasks = (List<Task>) request.getAttribute("opdrachten");
    %>
    <c:if test="${not empty tasks}">
        <table class="table">
            <tr>
                <th>id</th>
                <th>Title</th>
            </tr>
            <c:forEach items="${tasks}" var="task">
                <tr>
                    <td>${task.id}</td>
                    <td>${task.title}</td>
                </tr>
            </c:forEach>
        </table>
    </c:if>
    <c:if test="${empty tasks}">
        <p>All tasks have been eradicated, you can now retire</p>
    </c:if>
    <p>TEST</p>
</div>
<script>
    var $pageContent = $('body > div.container');
    $pageContent.hide();
    $(function () {
        $pageContent.fadeIn();
    });
</script>
</body>
</html>
