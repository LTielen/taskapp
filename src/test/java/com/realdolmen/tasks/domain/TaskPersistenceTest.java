package com.realdolmen.tasks.domain;


import com.realdolmen.tasks.AbstractPersistenceTest;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class TaskPersistenceTest extends AbstractPersistenceTest {
    @Test
    public void taskCanBePersisted() throws Exception {
        Task t = new Task();
        t.setTitle("bla bla");
        em.persist(t);
        assertNotNull(t.getId());
    }

    @Test
    public void taskCanBeLoaded() throws Exception {
        Task t = em.find(Task.class, 1000L);
        assertEquals(TaskStatus.INCOMPLETE, t.getStatus());
    }

    @Test
    public void allTasksCanBeLoaded(){
        List<Task> tasks = em.createQuery("select t from Task t where t.status = :status", Task.class).setParameter("status", TaskStatus.INCOMPLETE).getResultList();
        assertNotNull(tasks);
        assertEquals(2, tasks.size());
    }
}
