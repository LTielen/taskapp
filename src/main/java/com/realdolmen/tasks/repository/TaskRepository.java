package com.realdolmen.tasks.repository;

import com.realdolmen.tasks.domain.Task;
import com.realdolmen.tasks.domain.TaskStatus;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@ApplicationScoped
@Transactional
public class TaskRepository {
    @PersistenceContext
    EntityManager em;

    public Task findTaskById(long id){
        return em.find(Task.class, id);
    }

    public List<Task> findAllTasksByStatus(TaskStatus status){
        return em.createQuery("select t from Task t where t.status = :status", Task.class).setParameter("status", status).getResultList();
    }

    public void saveTask(Task task){
        em.persist(task);
    }
}
