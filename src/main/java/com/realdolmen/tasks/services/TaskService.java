package com.realdolmen.tasks.services;

import com.realdolmen.tasks.domain.Task;
import com.realdolmen.tasks.domain.TaskStatus;
import com.realdolmen.tasks.repository.TaskRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;

@ApplicationScoped
@Transactional
public class TaskService {
    @Inject
    private TaskRepository repo;

    public Task findTaskById(long id){
        return repo.findTaskById(id);
    }

    public List<Task> findAllTasksByStatus(TaskStatus status){
        return repo.findAllTasksByStatus(status);
    }

    public void saveTask(Task task){
        repo.saveTask(task);
    }
}
