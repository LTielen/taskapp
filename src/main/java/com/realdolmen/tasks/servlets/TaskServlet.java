package com.realdolmen.tasks.servlets;

import com.realdolmen.tasks.domain.Task;
import com.realdolmen.tasks.domain.TaskStatus;
import com.realdolmen.tasks.services.TaskService;

import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static java.lang.System.out;

@WebServlet(name = "TaskServlet", urlPatterns = "/home")
public class TaskServlet extends HttpServlet {
    @Inject
    private TaskService taskService;

    String address;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Task> tasks = taskService.findAllTasksByStatus(TaskStatus.INCOMPLETE);
        out.println("THE SIZE IS: " + tasks.size());
        request.setAttribute("opdrachten", tasks);
        address = "/WEB-INF/views/home.jsp";
        RequestDispatcher dispatcher = request.getRequestDispatcher(address);
        dispatcher.forward(request, response);
    }
}
