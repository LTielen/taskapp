package com.realdolmen.tasks.util;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;

@ApplicationScoped
public class TaskProducer {
    @PersistenceContext
    private EntityManager em;

    @Produces
    public EntityManager entityManager(){
        //emf = Persistence.createEntityManagerFactory("TaskPersistenceUnit");
        //em = emf.createEntityManager();
        return em;
    }
}
