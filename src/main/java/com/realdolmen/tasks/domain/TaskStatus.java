package com.realdolmen.tasks.domain;

public enum TaskStatus {
    INCOMPLETE, COMPLETE
}
